const fs = require('fs-extra');

(async () => {
  const src = './src/robots-dev.txt';
  const copy = './dist/browser/robots.txt';

  await fs.remove(copy);
  await fs.copy(src, copy);
})();
