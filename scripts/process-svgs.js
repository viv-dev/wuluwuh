const fs = require('fs');
const path = require('path');
const svgo = require('svgo');

const args = process.argv.slice(2);
const workingDir = process.cwd();

const svgoOptions = {
  plugins: [
    {
      prefixIds: true,
    },
    {
      removeDimensions: true,
    },
    {
      removeDimensions: true,
    },
  ],
};

const svgProcessor = new svgo(svgoOptions);

function processSvgFile(filepath, file) {
  const fullFilePath = path.join(filepath, file);
  const fileExt = path.extname(file);
  if (fileExt === '.svg') {
    fs.readFile(fullFilePath, 'utf8', function (err, data) {
      if (err) {
        throw err;
      }
      svgProcessor
        .optimize(data, { path: fullFilePath })
        .then((result) => {
          console.log(`Successfully processed ${file}`);
        })
        .catch((err) => {
          console.log(`Error processing ${file}. Error: ${err}`);
        });
    });
  } else {
    console.log(`${file} is not an svg file, skipping.`);
  }
}

/**
 * Supplied file / folder is taken from the working directory
 */
function processInput() {
  const filepath = path.resolve(workingDir, args[0]);
  const fileStats = fs.statSync(filepath);

  if (fileStats.isFile()) {
    processSvgFile(filepath);
  } else if (fileStats.isDirectory()) {
    fs.readdir(filepath, (err, files) => {
      files.forEach((file) => {
        processSvgFile(filepath, file);
      });
    });
  } else {
    console.log('What did you even give me...');
  }
}

processInput();
