// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCnihAAkGxG_5f-DadQqwGmD3OgumCRukQ',
    authDomain: 'wuluwuh-dev.firebaseapp.com',
    databaseURL: 'https://wuluwuh-dev.firebaseio.com',
    projectId: 'wuluwuh-dev',
    storageBucket: 'wuluwuh-dev.appspot.com',
    messagingSenderId: '453316930827',
    appId: '1:453316930827:web:3df5279b374cbb1dae060e',
  },
  noIndex: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
