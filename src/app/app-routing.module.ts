import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'lesbingo',
    redirectTo: '/lesbian-bingo',
    pathMatch: 'full',
  },
  {
    path: 'listercode',
    redirectTo: '/anne-lister-code',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      // Lazy loaded
      import('./features/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'lesbian-bingo',
    loadChildren: () =>
      import('./features/movie-bingo/movie-bingo.module').then(
        (m) => m.MovieBingoModule
      ),
  },
  {
    path: 'anne-lister-code',
    loadChildren: () =>
      import('./features/lister-code/lister-code.module').then(
        (m) => m.ListerCodeModule
      ),
  },
  {
    path: 'museum',
    loadChildren: () =>
      import('./features/museum/museum.module').then((m) => m.MuseumModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
