import {
  animate,
  query,
  style,
  transition,
  trigger,
  stagger,
  sequence,
} from '@angular/animations';
import { RouterOutlet } from '@angular/router';

/**
 * Function that prepares an outlet for triggering different animation states
 * depending on the animation meta data property on the router
 */
export function triggerRouteAnimation(outlet: RouterOutlet) {
  let animState = 'false';
  if (
    outlet &&
    outlet.activatedRouteData &&
    outlet.activatedRouteData.animation
  ) {
    animState = outlet.activatedRouteData.animation;
  }

  return animState;
}

/**
 * This is a class name that can be used in components to add additional sub-animation effects
 * as per the route animation definition below
 */
export const ROUTE_SUB_ANIMATION_SELECTOR = 'route-animations-elements';

/** The route animation definition */
export const routeAnimations = trigger('routeAnimations', [
  transition('void => *', []),
  transition('* => *', [
    query(':enter', style({ opacity: 0, position: 'fixed' }), {
      optional: true,
    }),
    query(':enter .' + ROUTE_SUB_ANIMATION_SELECTOR, style({ opacity: 0 }), {
      optional: true,
    }),
    sequence([
      query(
        ':leave',
        [
          style({ transform: 'translateY(0%)', opacity: 1 }),
          animate(
            '0.2s ease-in-out',
            style({ transform: 'translateY(-3%)', opacity: 0 })
          ),
          style({ position: 'fixed' }),
        ],
        { optional: true }
      ),
      query(
        ':enter',
        [
          style({
            transform: 'translateY(-3%)',
            opacity: 0,
            position: 'static',
          }),
          animate(
            '0.5s ease-in-out',
            style({ transform: 'translateY(0%)', opacity: 1 })
          ),
        ],
        { optional: true }
      ),
    ]),
    query(
      ':enter .' + ROUTE_SUB_ANIMATION_SELECTOR,
      stagger(75, [
        style({ transform: 'translateY(10%)', opacity: 0 }),
        animate(
          '0.5s ease-in-out',
          style({ transform: 'translateY(0%)', opacity: 1 })
        ),
      ]),
      { optional: true }
    ),
  ]),
]);
