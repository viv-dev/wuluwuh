import { TestBed } from '@angular/core/testing';

import { ListerTranslatorService } from './lister-translator.service';

describe('ListerTranslatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListerTranslatorService = TestBed.get(ListerTranslatorService);
    expect(service).toBeTruthy();
  });
});
