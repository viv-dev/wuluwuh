import { Injectable } from '@angular/core';

/** Describes the form a cipher map should take */
interface CipherMap {
  [key: string]: string;
}

@Injectable({
  providedIn: 'root',
})
export class ListerTranslatorService {
  /**
   * Provides a mapping to decode a coded letter into it's original
   * english equivalent
   */
  decodeList: CipherMap = {
    '2': 'a',
    '(': 'b',
    ')': 'c',
    '\u00B0': 'd',
    '3': 'e',
    v: 'f',
    n: 'g',
    o: 'h',
    θ: 'h',
    '4': '(i/j)',
    l: 'k',
    d: 'l',
    '-': 'm',
    '\\': 'n',
    '5': 'o',
    '+': 'p',
    '\u2225': 'q',
    p: 'r',
    '=': 's',
    '~': 't',
    '6': 'u',
    g: 'v',
    '8': 'w',
    w: 'x',
    '7': 'y',
    '9': 'z',
    '\u2713': 'th',
    '\u2227': 'sh',
    '\u2207': 'ch',
    '\u22F2': 'bb',
    '\u22FA': 'cc',
    ';': 'ee',
    q: 'ff',
    '\u0278': 'ff',
    φ: 'ff',
    ':': 'll',
    '\u22A5': 'nn',
    '!': 'oo',
    '\u2021': 'pp',
    '\u2647': 'rr',
    '?': 'ss',
    '\u2020': 'tt',
    x: 'miss',
    ẋ: 'mrs',
    ẍ: 'miss',
    '\u2718': 'and',
    '.': '.',
    ',': ',',
    '"': '"',
    "'": "'",
    '\n': '\n',
    '\t': '\t',
  };

  /**
   * Provides the mapping for all single letters to their corresponding
   * encoded value
   */
  singleLetters: CipherMap = {
    a: '2',
    b: '(',
    c: ')',
    d: '\u00B0',
    e: '3',
    f: 'v',
    g: 'n',
    h: 'θ',
    i: '4',
    j: '4',
    k: 'l',
    l: 'd',
    m: '-',
    n: '\\',
    o: '5',
    p: '+',
    q: '\u2225',
    r: 'p',
    s: '=',
    t: '~',
    u: '6',
    v: 'g',
    w: '8',
    x: 'w',
    y: '7',
    z: '9',
    ',': ',',
    '.': '.',
    '"': '"',
    "'": "'",
    '\n': '\n',
    '\t': '\t',
  };

  /**
   * Provides the mapping for all double letters to their corresponding
   * encoded value
   */
  doubleLetters: CipherMap = {
    th: '\u2713',
    sh: '\u2227',
    ch: '\u2207',
    bb: '\u22F2',
    cc: '\u22FA',
    ee: ';',
    // ff: '\u0278', // phi
    ff: 'φ', // lowercase phi
    ll: ':',
    nn: '\u22A5',
    oo: '!',
    pp: '\u2021',
    rr: '\u2647',
    ss: '?',
    tt: '\u2020',
  };

  /**
   * Provides the mapping for all special words to their corresponding
   * encoded value
   */
  words: CipherMap = {
    mr: 'x',
    mrs: '\u1E8B',
    miss: '\u1E8D',
    and: '\u2718',
  };

  constructor() {}

  /**
   * Creates a list of single letter mappings for rendering in the template
   * TODO: add memoisation
   */
  singleLetterCipher(): string[] {
    return this.createCipher(this.singleLetters);
  }

  /**
   * Creates a list of double letter mappings for rendering in the template
   * TODO: add memoisation
   */
  doubleLetterCipher(): string[] {
    return this.createCipher(this.doubleLetters);
  }

  /**
   * Creates a list of single letter mappings for rendering in the template
   * TODO: add memoisation
   */
  wordCipher(): string[] {
    return this.createCipher(this.words);
  }

  /**
   * From a supplied CipherMap, generates an array where each entry is
   * a string of the cipher key equaling the cipher value
   */
  private createCipher(object: CipherMap): string[] {
    const cipher = [];
    for (const key in object) {
      if (object.hasOwnProperty(key)) {
        if (key !== '\t' && key !== '\n') {
          cipher.push(`${key} = ${object[key]}`);
        }
      }
    }
    return cipher;
  }

  /**
   * Encodes an entire passage by first splitting the supplied string into
   * words, then encoding each word seperately, and finally stitching the encoded
   * words back together into a single encoded passage.
   *
   * TODO: test how this would behave if words have more than one space seperating them
   */
  encode(passage: string): string {
    const words: string[] = passage.split(' ');
    const encodedWords: string[] = words.map((word) => this.encodeWord(word));
    const encodedPassage: string = encodedWords.reduce(
      (prevValue, currentValue) => prevValue + ' ' + currentValue
    );

    return encodedPassage;
  }

  /** Takes in a single word and attempts to encode it*/
  private encodeWord(word: string): string {
    const normalizedWord = word.toLowerCase();

    // If it's a special word return it's encoded value
    if (normalizedWord in this.words) {
      return this.words[normalizedWord];
    }

    // Otherwise grab the individual letters
    const letters: string[] = normalizedWord.split('');
    let encodedWord = '';

    // And process them individually
    let i = 0;
    while (i < letters.length) {
      const isLastLetter: boolean = i + 1 === letters.length;
      const letter = letters[i];

      // Check if it's a double letter symbol
      let isDouble = false;
      if (!isLastLetter) {
        const doubleLetter = letter + letters[i + 1];
        if (doubleLetter in this.doubleLetters) {
          encodedWord += this.doubleLetters[doubleLetter];
          i = i + 2;
          isDouble = true;
        }
      }

      // Otherwise
      if (!isDouble) {
        if (letter in this.singleLetters) {
          encodedWord = encodedWord + this.singleLetters[letter];
        } else {
          console.log(`Untranslatable character: ${letter}`);
        }
        i++;
      }
    }

    return encodedWord;
  }

  /**
   * Decodes an entire passage by first splitting the supplied string into
   * words, then decodes each word seperately, and then finally stitches the decoded
   * words back together into a single decoded passage.
   *
   * TODO: test how this would behave if words have more than one space seperating them
   */
  decode(passage: string) {
    const words: string[] = passage.split(' ');
    const decodedWords: string[] = words.map((word) => this.decodeWord(word));
    const decodedPassage: string = decodedWords.reduce(
      (prevValue, currentValue) => prevValue + ' ' + currentValue
    );

    return decodedPassage;
  }

  /**
   * Takes in a single word and attempts to decode it
   */
  private decodeWord(word: string) {
    const normalizedWord = word.toLowerCase();

    const letters: string[] = normalizedWord.split('');

    let translatedWord = '';
    for (const letter of letters) {
      if (letter in this.decodeList) {
        translatedWord += this.decodeList[letter];
      }
    }
    return translatedWord;
  }
}
