import { Injectable } from '@angular/core';
import { Meta, Title, MetaDefinition } from '@angular/platform-browser';

export interface PageMetaData {
  title: string;
  description: string;
  keywords: string;
  url: string;
  imageUrl: string;
}

@Injectable({
  providedIn: 'root',
})
export class SeoService {
  constructor(private meta: Meta, private title: Title) {}

  setMetaData(pageMeta: PageMetaData) {
    this.updateTitle(pageMeta.title);
    this.setMetaTags([
      { name: 'title', content: pageMeta.title },
      { name: 'description', content: pageMeta.description },
      { name: 'keywords', content: pageMeta.keywords },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: '@wuluwuh' },
      { name: 'twitter:title', content: pageMeta.title },
      { name: 'twitter:description', content: pageMeta.description },
      { name: 'twitter:text:description', content: pageMeta.description },
      { name: 'twitter:image', content: pageMeta.imageUrl },
      { name: 'twitter:url', content: pageMeta.url },
    ]);

    this.setOpenGraphTags([
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: pageMeta.url },
      { property: 'og:title', content: pageMeta.title },
      { property: 'og:description', content: pageMeta.description },
      { property: 'og:image', content: pageMeta.imageUrl },
    ]);
  }

  addNoIndex() {
    this.meta.addTag({ name: 'robos', content: 'noindex' });
  }

  private updateTitle(title: string): void {
    this.title.setTitle(title);
  }

  private setMetaTags(tags: MetaDefinition[]) {
    tags.forEach((tag) => {
      if (this.meta.getTag(`name="${tag.name}"`)) {
        this.meta.updateTag(tag);
      } else {
        this.meta.addTag(tag);
      }
    });
  }

  private setOpenGraphTags(tags: MetaDefinition[]) {
    tags.forEach((tag) => {
      if (this.meta.getTag(`property="${tag.property}"`)) {
        this.meta.updateTag(tag);
      } else {
        this.meta.addTag(tag);
      }
    });
  }
}
