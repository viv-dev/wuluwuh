import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

interface QueerWoman {
  name: string;
  description: string;
  dateBorn: Date;
  dateDied: Date;
}

@Injectable({
  providedIn: 'root',
})
export class MuseumService {
  mockData = new Map<string, QueerWoman>([
    [
      'sappho',
      {
        name: 'Sappho',
        description: 'A woman from greece',
        dateBorn: new Date(),
        dateDied: new Date(),
      },
    ],
    [
      'lister',
      {
        name: 'Anne Lister',
        description: 'A woman from England',
        dateBorn: new Date(),
        dateDied: new Date(),
      },
    ],
  ]);

  constructor() {}

  getQueerWoman(womanId: string): Observable<QueerWoman> {
    return of(this.mockData.get(womanId));
  }
}
