import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BingoBoardPageComponent } from './bingo-board-page.component';
import { PageMetaData } from '@app/core';

const pageMeta: PageMetaData = {
  title:
    'Lesbian Movie Bingo - battle those lesbian tropes and find that bingo!',
  // tslint:disable-next-line:max-line-length
  description: `Grab some popcorn, load up that B-grade wlw movie, and get ready to hunt for those Lesbian movie tropes! In this particular Bingo game, the objective is to try and make any row, column, or diagonal light up for that bitter-sweet Bingo!`,
  keywords:
    'lesbian, bingo, movie, queer cinema, lesbian cinema, tropes, gay tropes',
  url: 'https://wuluwuh.com/lesbingo',
  imageUrl: 'https://wuluwuh.com/assets/images/lesbingo-min.png',
};

const movieBingoRoutes: Routes = [
  {
    path: '',
    component: BingoBoardPageComponent,
    data: {
      animation: 'LesBingoPage',
      metaData: pageMeta,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(movieBingoRoutes)],
  exports: [RouterModule],
})
export class MovieBingoRoutingModule {}
