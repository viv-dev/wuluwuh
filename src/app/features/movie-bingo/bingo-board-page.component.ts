import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
  GoogleAnalyticsService,
  ModalOverlayRef,
  ModalService,
} from '@app/core';
import { ROUTE_SUB_ANIMATION_SELECTOR } from '@app/core/animations/route.animations';
import { InfoBoxModalComponent } from './modals/info-box/info-box-modal.component';

interface BingoCard {
  text: string;
  active: boolean;
}

const BOARD_DIMENSIONS = 5;
const TOTAL_BOARD_ITEMS = BOARD_DIMENSIONS * BOARD_DIMENSIONS;
const NO_OF_LIGHTS = 52;

@Component({
  selector: 'app-bingo-board',
  templateUrl: './bingo-board-page.component.html',
  styleUrls: ['./bingo-board-page.component.scss'],
  providers: [ModalService],
})
export class BingoBoardPageComponent implements OnInit, AfterViewInit {
  /** Used to make an element be animated on route changes */
  routeAnimationElement = ROUTE_SUB_ANIMATION_SELECTOR;

  /** Used to control the modals created by this page */
  modalRef: ModalOverlayRef;

  /** All of the possible bingo cards */
  bingoItems: BingoCard[] = [
    { text: 'The Spontaneous Outdoor Swim', active: false },
    { text: 'Older Woman / Younger Woman', active: false },
    { text: 'Cheating', active: false },
    { text: 'Straight Sex Blank Stare', active: false },
    { text: 'Bath Scene', active: false },
    { text: 'Horses', active: false },
    { text: 'Gay Person Dies', active: false },
    { text: 'Thats NOT how Lesbian Sex Works', active: false },
    { text: 'Blonde and Brunette', active: false },
    { text: "Let's Run Away Together", active: false },
    { text: "I've Never Done This Before", active: false },
    { text: 'Femmes Everywhere!', active: false },
    { text: 'Creepy Straight Guy', active: false },
    { text: 'Touching Foreheads', active: false },
    { text: 'Predatory Lesbian', active: false },
    { text: 'Family Love Triangle', active: false },
    { text: 'Discreet Public Hand Holding', active: false },
    { text: 'Intense Staring', active: false },
    { text: 'Hair Stroking', active: false },
    { text: 'Angsty Girl Soundtrack', active: false },
    { text: 'Going Back to the Guy', active: false },
    { text: 'Just Gal Pals', active: false },
    { text: 'Photography', active: false },
    { text: 'Male Gaze', active: false },
    { text: 'Artsy/Edgy Smoking', active: false },
  ];

  /**
   * Two dimensional array that represents the state of the board.
   */
  board: BingoCard[][] = [];

  /**
   * Array that holds, for each row (i.e. index), the count of the
   * number of items selected in that row (i.e. the value of the index)
   */
  rowCount: number[] = [];

  /**
   * Array that holds, for each column (i.e. index), the count of the
   * number of items selected in that column (i.e. the value of the index)
   */
  columnCount: number[] = [];

  /** The number of selected items across the left-right diagonal */
  diag1Count: number;

  /** The number of selected items across the right-left diagonal */
  diag2Count: number;

  /** Flag indicating if a user has gotten bingo */
  bingo: Boolean = false;

  /** Flag to control */
  displayInfo: Boolean = false;

  /**
   * Dummy array used to generate all the elements that represent lights
   * around the outside of the board
   */
  lights: number[] = [];

  constructor(
    public gaService: GoogleAnalyticsService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.resetScore();
    this.initLights();
    this.initBoard();
  }

  ngAfterViewInit() {}

  resetScore() {
    for (let i = 0; i < BOARD_DIMENSIONS; i++) {
      if (this.rowCount.length < BOARD_DIMENSIONS) {
        this.rowCount.push(0);
      } else {
        this.rowCount[i] = 0;
      }

      if (this.columnCount.length < BOARD_DIMENSIONS) {
        this.columnCount.push(0);
      } else {
        this.columnCount[i] = 0;
      }
    }

    this.diag1Count = 0;
    this.diag2Count = 0;
  }

  initLights() {
    for (let i = 0; i < NO_OF_LIGHTS; i++) {
      this.lights.push(i);
    }
  }

  initBoard() {
    this.shuffle(this.bingoItems);
    for (let i = 0; i < BOARD_DIMENSIONS; i++) {
      this.board.push(this.getRow(i, this.bingoItems));
    }
  }

  getRow(row: number, bindoItems: BingoCard[]) {
    if (bindoItems.length !== TOTAL_BOARD_ITEMS) {
      console.error('Board does not contain the right amount of items.');
      return;
    }

    if (row < 0 || row >= BOARD_DIMENSIONS) {
      console.error(
        `Invalid row. Number must be between 0 and ${BOARD_DIMENSIONS - 1}`
      );
    }

    const rowIndex: number = BOARD_DIMENSIONS * row;
    return bindoItems.slice(rowIndex, rowIndex + BOARD_DIMENSIONS);
  }

  clearBoard() {
    this.board.forEach((row) => row.forEach((card) => (card.active = false)));
  }

  reset() {
    this.initBoard();
    this.resetScore();
    this.clearBoard();
  }

  shuffle(array: BingoCard[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  checkBingo(row: number, col: number, active: boolean) {
    if (active) {
      this.rowCount[row]++;
      this.columnCount[col]++;

      if (row === col) {
        this.diag1Count++;
      }

      if (row + col === 4) {
        this.diag2Count++;
      }

      if (
        this.rowCount[row] === 5 ||
        this.columnCount[col] === 5 ||
        this.diag1Count === 5 ||
        this.diag2Count === 5
      ) {
        this.setBingo(true);
      }
    } else {
      this.rowCount[row]--;
      this.columnCount[col]--;

      if (row === col) {
        this.diag1Count--;
      }

      if (row + col === 4) {
        this.diag2Count--;
      }
    }
  }

  toggleCard(item: BingoCard, row: number, col: number) {
    item.active = !item.active;

    this.checkBingo(row, col, item.active);

    const pos = (row + 1) * (col + 1);
    if (item.active) {
      this.gaService.eventEmitter('bingoClick', item.text, pos.toString());
    }
  }

  setBingo(value: Boolean) {
    if (this.displayInfo) {
      this.displayInfo = false;
    }
    this.bingo = value;

    if (value) {
      const audio = new Audio();
      audio.src = 'assets/sound/depressed_fireworks.mp3';
      audio.load();
      audio.play();

      this.gaService.eventEmitter('gameEvent', 'bingo');
    }
  }

  setDisplayInfo(value: Boolean) {
    if (this.bingo) {
      this.bingo = false;
    }

    this.displayInfo = value;
    this.modalRef = this.modalService.open(InfoBoxModalComponent);

    if (value) {
      this.gaService.eventEmitter('bingoClick', 'info');
    }
  }

  resetBoard() {
    this.gaService.eventEmitter('bingoClick', 'refresh');

    this.reset();
  }
}
