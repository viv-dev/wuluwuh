import { Component, OnInit } from '@angular/core';
import { ModalOverlayRef } from '@app/core';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box-modal.component.html',
  styleUrls: ['./info-box-modal.component.scss'],
})
export class InfoBoxModalComponent implements OnInit {
  constructor(private modalOverlay: ModalOverlayRef) {}

  ngOnInit() {}

  close() {
    this.modalOverlay.close();
  }
}
