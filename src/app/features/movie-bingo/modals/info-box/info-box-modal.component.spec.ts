import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoBoxModalComponent } from './info-box-modal.component';

describe('InfoBoxComponent', () => {
  let component: InfoBoxModalComponent;
  let fixture: ComponentFixture<InfoBoxModalComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [InfoBoxModalComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoBoxModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
