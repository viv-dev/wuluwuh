import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { BingoBoardPageComponent } from './bingo-board-page.component';

describe('BingoBoardComponent', () => {
  let component: BingoBoardPageComponent;
  let fixture: ComponentFixture<BingoBoardPageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [BingoBoardPageComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(BingoBoardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
