// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { FlexLayoutModule } from '@angular/flex-layout';

// Third Party
import { NgScrollbarModule } from 'ngx-scrollbar';

// Local Imports
import { MovieBingoRoutingModule } from './movie-bingo-routing.module';
import { BingoBoardPageComponent } from './bingo-board-page.component';
import { InfoBoxModalComponent } from './modals/info-box/info-box-modal.component';

@NgModule({
  declarations: [BingoBoardPageComponent, InfoBoxModalComponent],
  imports: [
    CommonModule,
    MovieBingoRoutingModule,
    FlexLayoutModule,
    NgScrollbarModule,
    OverlayModule,
    PortalModule,
  ],
})
export class MovieBingoModule {}
