import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page.component';
import { PageMetaData } from '@app/core';

const pageMeta: PageMetaData = {
  title: 'Wuluwuh - sharing the wlw experience through online content',
  // tslint:disable-next-line:max-line-length
  description:
    'Welcome to our space of the internet, where a couple of queer women deliver novel games, tools, and diy gadget and tech tutorials focused on representing the experiences of Women who Love Women (WLW or wuluwuh!)',
  keywords:
    'lgbta, queer, women loving women, wlw, lesbian, bi, lesbian content',
  url: 'https://wuluwuh.com',
  imageUrl: 'https://wuluwuh.com/assets/images/wlwomen.png',
};

const homeRoutes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    data: {
      animation: 'HomePage',
      metaData: pageMeta,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
