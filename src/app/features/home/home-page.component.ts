import { Component, OnInit } from '@angular/core';
import { ROUTE_SUB_ANIMATION_SELECTOR } from '@app/core/animations/route.animations';
import { GoogleAnalyticsService } from '@app/core';

@Component({
  selector: 'app-home',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  routeAnimationElement = ROUTE_SUB_ANIMATION_SELECTOR;

  constructor(private gaService: GoogleAnalyticsService) {}

  ngOnInit() {}

  logMailTo() {
    this.gaService.eventEmitter('homeClick', 'say_hello');
  }

  logTwitter() {
    this.gaService.eventEmitter('homeClick', 'twitter');
  }
}
