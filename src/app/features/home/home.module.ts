// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { FlexLayoutModule } from '@angular/flex-layout';

// Third Party
import { NgScrollbarModule } from 'ngx-scrollbar';

// Local imports
import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './home-page.component';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FlexLayoutModule,
    NgScrollbarModule,
    OverlayModule,
    PortalModule,
  ],
})
export class HomeModule {}
