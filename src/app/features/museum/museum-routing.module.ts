import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageMetaData } from '@app/core';
import { MuseumPageComponent } from './museum-page.component';
import { ExhibitionPageComponent } from './pages/exhibition-page/exhibition-page.component';

const pageMeta: PageMetaData = {
  title: 'Museum of Queer Women',
  description: '',
  keywords: '',
  url: 'https://wuluwuh.com/museum',
  imageUrl: '',
};

const museumRoutes: Routes = [
  {
    path: '',
    component: MuseumPageComponent,
    data: {
      animation: 'MuseumPage',
      metaData: pageMeta,
    },
  },
  {
    path: ':queer-woman',
    component: ExhibitionPageComponent,
    data: {
      animation: 'ExhibitionPage',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(museumRoutes)],
  exports: [RouterModule],
})
export class MuseumRoutingModule {}
