import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MuseumRoutingModule } from './museum-routing.module';
import { MuseumPageComponent } from './pages/museum-page/museum-page.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExhibitionPageComponent } from './pages/exhibition-page/exhibition-page.component';

@NgModule({
  declarations: [MuseumPageComponent, ExhibitionPageComponent],
  imports: [CommonModule, MuseumRoutingModule, FlexLayoutModule],
})
export class MuseumModule {}
