import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListerCodePageComponent } from './lister-code-page.component';

describe('ListerCodePageComponent', () => {
  let component: ListerCodePageComponent;
  let fixture: ComponentFixture<ListerCodePageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ListerCodePageComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListerCodePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
