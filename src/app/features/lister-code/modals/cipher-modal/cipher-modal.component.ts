import { Component, OnInit } from '@angular/core';
import { ModalOverlayRef } from '@app/core';
import { ListerTranslatorService } from '@app/core/services/lister-translator.service';

@Component({
  selector: 'app-cipher',
  templateUrl: './cipher-modal.component.html',
  styleUrls: ['./cipher-modal.component.scss'],
})
export class CipherModalComponent implements OnInit {
  /** Array of strings that represent the mappings of single letters */
  singleLetterCipher: string[] = [];

  /** Array of strings that represent the mapping of double letters */
  doubleLetterCipher: string[] = [];

  /** Array of strings that represent the mapping of entire words */
  wordCipher: string[] = [];

  constructor(
    private translator: ListerTranslatorService,
    private modalOverlay: ModalOverlayRef
  ) {}

  ngOnInit() {
    // Request from the translator service the mappings
    this.singleLetterCipher = this.translator.singleLetterCipher();
    this.doubleLetterCipher = this.translator.doubleLetterCipher();
    this.wordCipher = this.translator.wordCipher();
  }

  close() {
    this.modalOverlay.close();
  }
}
