import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListerCodePageComponent } from './lister-code-page.component';
import { PageMetaData } from '@app/core';

const pageMeta: PageMetaData = {
  title: 'Anne Lister Diary Encoder/Decoder - translate her journal code',
  // tslint:disable-next-line:max-line-length
  description:
    "Encrypt and decrypt text to and from Anne Lister's real life 1800s diary code - as seen on HBO and BBC TV series Gentleman Jack",
  keywords:
    'Anne, Lister, Gentleman Jack, code, encode, decode, cypher, cipher, encrypt, decrypt, translate, journal, diary',
  url: 'https://wuluwuh.com/listercode',
  imageUrl: 'https://wuluwuh.com/assets/images/listercode-min.png',
};

const listerRoutes: Routes = [
  {
    path: '',
    component: ListerCodePageComponent,
    data: {
      animation: 'ListerCodePage',
      metaData: pageMeta,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(listerRoutes)],
  exports: [RouterModule],
})
export class ListerCodeRoutingModule {}
