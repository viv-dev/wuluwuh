// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { FlexLayoutModule } from '@angular/flex-layout';

// Third Party
import { ClipboardModule } from 'ngx-clipboard';
import { NgScrollbarModule } from 'ngx-scrollbar';

// Local Imports
import { ListerCodeRoutingModule } from './lister-code-routing.module';
import { ListerCodePageComponent } from './lister-code-page.component';
import { CipherModalComponent } from './modals/cipher-modal/cipher-modal.component';

@NgModule({
  declarations: [ListerCodePageComponent, CipherModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ListerCodeRoutingModule,
    ClipboardModule,
    FlexLayoutModule,
    NgScrollbarModule,
    OverlayModule,
    PortalModule,
  ],
})
export class ListerCodeModule {}
