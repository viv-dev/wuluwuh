import { Component, OnInit } from '@angular/core';
import { ListerTranslatorService } from '@app/core/services/lister-translator.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ModalService,
  ModalOverlayRef,
  GoogleAnalyticsService,
} from '@app/core';
import { CipherModalComponent } from './modals/cipher-modal/cipher-modal.component';
import { ROUTE_SUB_ANIMATION_SELECTOR } from '@app/core/animations/route.animations';

@Component({
  selector: 'app-translator',
  templateUrl: './lister-code-page.component.html',
  styleUrls: ['./lister-code-page.component.scss'],
})
export class ListerCodePageComponent implements OnInit {
  /** Used to make an element be animated on route changes */
  routeAnimationElement = ROUTE_SUB_ANIMATION_SELECTOR;

  /** Angular reactive form group for extracting data from the encoder input */
  inputTextForm: FormGroup;

  /** The encoded/decoded text to display */
  outputText: string;

  /** Stores the reference to the cipher modal whenever it is opened */
  cipherModalRef: ModalOverlayRef;

  /** Status boolean for the copy button */
  copySuccess = false;

  constructor(
    private translator: ListerTranslatorService,
    private formBuilder: FormBuilder,
    private modalService: ModalService,
    private gaService: GoogleAnalyticsService
  ) {
    // Set up the form using the form builder utility
    this.inputTextForm = this.formBuilder.group({
      inputText: ['', Validators.required],
    });
  }

  ngOnInit() {}

  /** Getter utility for accessing the input text field in the form group */
  get inputText() {
    return this.inputTextForm.get('inputText');
  }

  /** Invokes the translator service to encode the current text in the input box */
  encode() {
    this.outputText = this.translator.encode(this.inputText.value);

    this.gaService.eventEmitter('listerClick', 'encode');
  }

  /** Invokes the translator service to attempt to decode the current text in the input box */
  decode() {
    this.outputText = this.translator.decode(this.inputText.value);
    this.gaService.eventEmitter('listerClick', 'decode');
  }

  /** Opens the cipher modal to show what the different letters map to */
  showCipher() {
    this.cipherModalRef = this.modalService.open(CipherModalComponent);
    this.gaService.eventEmitter('listerClick', 'seeCypher');
  }

  /** Fire google analytics event to external info url */
  logListerInfo() {
    this.gaService.eventEmitter('listerClick', 'listerInfo');
  }

  /** Fire google analytics event to external book url */
  logBookInfo() {
    this.gaService.eventEmitter('listerClick', 'bookInfo');
  }

  /** Fire google analytics event when the copy text button is clicked */
  logCopyText() {
    this.gaService.eventEmitter('listerClick', 'copyText');
  }

  /**
   * Sets the copy success flag to true in order to show a success message to
   * the user for 3 seconds when the text is successfully copied
   */
  copied(event) {
    if (event.isSuccess) {
      this.copySuccess = true;

      setTimeout(() => {
        this.copySuccess = false;
      }, 3000);
    }
  }
}
