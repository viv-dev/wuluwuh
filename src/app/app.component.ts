import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {
  routeAnimations,
  triggerRouteAnimation,
} from '@app/core/animations/route.animations';
import { SeoService } from './core/services/seo.service';
import { filter, map, mergeMap } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

import { environment } from '../environments/environment';

declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimations],
})
export class AppComponent implements OnInit {
  title = 'Wuluwuh';

  triggerRouteAnimation = triggerRouteAnimation;

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private seoService: SeoService,
    @Inject(PLATFORM_ID) private platformId
  ) {}

  ngOnInit() {
    // If we're running in the browser then trigger a google analytics page view event
    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', event.urlAfterRedirects);
          ga('send', 'pageview');
        }
      });
    }

    // Dynamically change the html meta tags based on the current route state
    this.router.events // Subcribe to all events in the  router
      .pipe(
        filter((event) => event instanceof NavigationEnd), // If the event type is notifying the navigation to the new route has finished
        map(() => this.activatedRoute), // Swap the event for the current activated route
        map((activatedRoute) => {
          //
          while (activatedRoute.firstChild) {
            activatedRoute = activatedRoute.firstChild;
          }
          return activatedRoute;
        }),
        filter((route) => route.outlet === 'primary'),
        mergeMap((route) => route.data)
      )
      .subscribe((routeData) => {
        this.seoService.setMetaData(routeData['metaData']);
      });

    if (!environment.production) {
      this.seoService.addNoIndex();
    }
  }
}
