import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeartLoaderComponent } from './heart-loader.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [HeartLoaderComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [HeartLoaderComponent],
})
export class HeartLoaderModule {}
