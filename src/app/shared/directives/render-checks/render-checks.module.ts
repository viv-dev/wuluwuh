import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellNoRenderDirective } from './app-shell-no-render.directive';
import { AppShellRenderDirective } from './app-shell-render.directive';

@NgModule({
  declarations: [AppShellNoRenderDirective, AppShellRenderDirective],
  imports: [CommonModule],
  exports: [AppShellNoRenderDirective, AppShellRenderDirective],
})
export class RenderChecksModule {}
